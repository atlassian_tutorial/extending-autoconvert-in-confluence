# Tutorial: Extending Confluence Autoconvert

This tutorial shows you how to create a plugin that extends Confluence Autoconvert. 
Your plugin converts URL text into hyperlinked title text. The conversion appears 
dynamically as you edit in Confluence and add URLs from the http://developer.atlassian.com 
domain.

You can read the full tutorial at: [Extending Autoconvert][1].

## Running locally

To run this app locally, make sure that you have the [Atlassian Plugin SDK][2] installed, and then run:

* atlas-run   -- installs this plugin into the product and starts it on localhost
* atlas-debug -- same as atlas-run, but allows a debugger to attach at port 5005
* atlas-cli   -- after atlas-run or atlas-debug, opens a Maven command line window:
                 - 'pi' reinstalls the plugin into the running product instance
* atlas-help  -- prints description for all commands in the SDK
    
 [1]: https://developer.atlassian.com/confdev/tutorials/extending-autoconvert
 [2]: https://developer.atlassian.com/docs/getting-started/set-up-the-atlassian-plugin-sdk-and-build-a-project